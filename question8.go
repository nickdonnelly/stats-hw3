package main

import (
	"math"
)

func Question8A() float64{
    // n could be any value larger than this
	n := (1/(2*math.Pow(0.005, 2)))*math.Log(2/0.05)
	countGreater := 0.0 // counts the number of times x > y
	for i := 0.0; i < n; i+=1.0{
		x := PoissonRand(4.0)
		y := PoissonRand(5.0)

		if x > y{
			countGreater += 1.0
		}
	}

	// Proportion of times x > y
	return countGreater / n
}

// Generates a single random Poisson variable with parameter lambda.
func PoissonRand(lambda float64) float64{
	u := randGen()
	cp := math.Exp(-1.0 * lambda)
	k := 0.0
	fac_k := 1.0
	for cp < u {
		k += 1
		fac_k = fac_k * k
		cp = cp + math.Exp(-1.0 * lambda) * math.Pow(lambda, k)/fac_k
	}
	return k
}