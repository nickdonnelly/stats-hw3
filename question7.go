package main

import (
	// "math"
)

func Question7A() float64{
	a := 0.0
	b := 1.0
	c := 1.5 + 1 // the max of the function btwn -1 and 1
	u := randGen() // random uniform vars
	v := randGen()
	X := a + (b - a)*u
	Y := c * v

	for Y > Q6PDF(X){
		u = randGen()
		v = randGen()
		X =  a + (b - a)*u
		Y = c * v
	}
	return X
}