package main

import (
	"math"
)

func Question6A() float64{
	// For 10000 samples
	// Simply average each trial together.
	runningCount := 0.0
	for x := 0; x < 10000; x++{
		runningCount += Q6RandVal(randGen())
	}
	runningCount = runningCount / 10000
	return runningCount
}

// Generates a random variable from a uniform value x.
// This is the inverse function.
func Q6RandVal(u float64) float64{
	// Solved u=x^3/8 + 3x^2/8 + 3x/8 for x to get this
	return math.Pow((8.0 * u) + 1, 1.0/3.0) - 1
}

func Q6PDF(x float64) float64{
	if x < -1 || x > 1{
		return 0
	}
	return (3.0/8.0)*math.Pow(x+1, 2)
}

// The CDF from question 6
func Q6CDF(x float64) float64{
	if x < -1{
		return 0
	}else if x > 1{
		return 1
	}
	return math.Pow(x, 3)/8 + (3*math.Pow(x, 2))/8 + (3*x)/8
}