package main

import (
)

func Question9A() float64{
	currentlyInfected := 1.0
	p := 0.15
	t := 1.0 // Day count, starting at 1
	for currentlyInfected > 0.0 {
		if currentlyInfected >= 1.0{
			// For each uninfected machine, sum a bernoulli RV
			for i := 0.0; i < (40.0 - currentlyInfected); i += 1.0{
				currentlyInfected += Bernoulli(p)
			}
		}
		if t > 1.0 && currentlyInfected > 5.0{
			currentlyInfected = currentlyInfected - 5.0
		}else if t > 1.0{
			currentlyInfected = 0.0
		}
		t += 1.0
	}
	return t
}

func Question9B() float64{
	n := 100000.0
	p := 0.15
	greaterThan20Count := 0.0
	for i := 1.0; i < n; i += 1.0{
		// X := 39.0 // initially 1 PC infected
		// Y := 40.0 - X
		g := GeometricVal(p)
		if g > 20{
			greaterThan20Count += 1.0
		}
	}
	return greaterThan20Count / n
}

func GeometricVal(p float64) float64{
	x := 1.0
	for Bernoulli(p)==0{
		x += 1.0
	}
	return x
}

func Question9C() float64{
  n := 1000000.0
  p := 0.15
  startVal := 0.0
  for i := 0.0; i < n; i++ {
    g := GeometricVal(p)
    startVal += g
  }

	return (startVal / n)
}

