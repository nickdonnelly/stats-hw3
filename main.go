package main

import(
	"fmt"
	"math/rand"
)

func main(){
	fmt.Print("Question 6A: ", Question6A(), "\n")
	fmt.Print("Question 7A: ", Question7A(), "\n")
	fmt.Print("Question 8A: ", Question8A(), "\n")
	fmt.Print("Question 9A: ", Question9A(), " days \n")
	fmt.Print("Question 9B: ", Question9B(), "\n")
	fmt.Print("Question 9C: ", Question9C(), "\n")
  Question10E()
}

func randGen() float64{
	// rand.Seed(time.Now().UTC().UnixNano())
	return rand.Float64()	
}

func Bernoulli(p float64) float64{
	if randGen() < p{
		return 1
	}
	return 0
}
