package main

import(
  "fmt"
  "sort"
  "math"
)

func Question10E(){
  vals := [...]float64{
    84, 49, 61, 40, 83, 67, 45, 66, 70, 69, 80, 58, 68, 60, 67, 72, 73, 70, 57, 63, 70, 78, 52, 67, 53, 67, 75, 61, 70, 81, 76, 79, 75, 76, 58, 31,
  }
  
  for i , v := range vals {
    vals[i] = (5.0/9.0) * (v - 32.0)
  }

  meanCounter := 0.0
  n := 0.0
  for _, v := range vals{
    meanCounter += v
    n += 1.0
  }
  
  sort.Float64s(vals[:])

  fmt.Println("Question 10E:MEDIAN: ", (vals[19] + vals[18])/2.0)

  stdDevSum := 0.0

  for _, v := range vals{
    stdDevSum += math.Pow((v - (meanCounter / n)), 2)
  }

  fmt.Println("Question 10E:STDDEV: ", math.Pow(stdDevSum / n, 0.5))

  fmt.Println("Question 10E:MEAN: ",  meanCounter / n)

  fmt.Println("Question 10E:VALS: ", vals)

}
